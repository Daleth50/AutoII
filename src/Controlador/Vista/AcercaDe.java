package Controlador.Vista;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class AcercaDe extends JFrame
{
	JLabel roge,erika,tavo,pedro,iroge,ierika,itavo,ipedro,i;
	ImageIcon img1,fotor,fotoe,fotot,fotop,infr,infe,inft,infp;
	
	public AcercaDe() 
	{
		setSize(900,600);
		setTitle("ACERCA DE...");
		
		//this.getContentPane().setBackground(Color.white);
		URL ruta=getClass().getResource("");
		Toolkit tk=Toolkit.getDefaultToolkit();
		Image icono=tk.getImage(ruta);
		setIconImage(icono);
		
		ruta=getClass().getResource("/conocenos2.png");
		img1=new ImageIcon(ruta);
		
		//Erika
		
		ruta=getClass().getResource("/erika.jpg");
		fotoe=new ImageIcon(ruta);
		ruta=getClass().getResource("/inf1.png");
		infe=new ImageIcon(ruta);
		
		//Rogelio
		ruta=getClass().getResource("/roge.jpg");
		fotor=new ImageIcon(ruta);
		ruta=getClass().getResource("/inf2.png");
		infr=new ImageIcon(ruta);
		
		//Pedro
		ruta=getClass().getResource("/pedro.jpg");
		fotop=new ImageIcon(ruta);
		ruta=getClass().getResource("/inf3.png");
		infp=new ImageIcon(ruta);
		
		//Tavo
		ruta=getClass().getResource("/tavo.jpg");
		fotot=new ImageIcon(ruta);
		ruta=getClass().getResource("/inf4.png");
		inft=new ImageIcon(ruta);
		
		i=new JLabel(img1);
		roge=new JLabel(fotoe);
		erika=new JLabel(fotor);
		pedro=new JLabel(fotop);
		tavo=new JLabel(fotot);
		iroge=new JLabel(infr);
		ierika=new JLabel(infe);
		ipedro=new JLabel(infp);
		itavo=new JLabel(inft);
		
		
		JPanel arriba=new JPanel();
		arriba.setLayout(new GridLayout(1,1));
		arriba.add(i);
		add(arriba,BorderLayout.NORTH);
		
		JPanel centro=new JPanel();
		centro.setLayout(new GridLayout(1,4));
		centro.add(erika);
		centro.add(roge);
		centro.add(pedro);
		centro.add(tavo);
		add(centro,BorderLayout.CENTER);
		
		JPanel abajo=new JPanel();
		abajo.setLayout(new GridLayout(1,4));
		abajo.add(ierika);
		abajo.add(iroge);
		abajo.add(ipedro);
		abajo.add(itavo);
		add(abajo,BorderLayout.SOUTH);
		
		setVisible(true);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
	}
	
	public static void main(String[] args)
	{
		new AcercaDe();
	}

}

